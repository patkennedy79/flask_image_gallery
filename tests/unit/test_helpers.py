"""
This file (test_helpers.py) contains the unit tests for the helper functions in routes.py file.
"""
from project.users.routes import is_safe_url


def test_is_safe_url():
    """
    URL: /logout
    Result: Safe (True)
    """
    assert is_safe_url('/logout')

    """
    URL: logout
    Result: Illegal (False)
    """
    assert not is_safe_url('logout')

    """
    URL: /logout2
    Result: Safe (True)
    """
    assert is_safe_url('/logout2')

    """
    URL: www.google.com
    Result: Illegal (False)
    """
    assert not is_safe_url('www.google.com')

    """
    URL: http://www.google.com
    Result: Illegal (False)
    """
    assert not is_safe_url('http://www.google.com')

    """
    URL: ftp.google.com
    Result: Illegal (False)
    """
    assert not is_safe_url('ftp.google.com')

    """
    URL: ftp://123.456.789
    Result: Illegal (False)
    """
    assert not is_safe_url('ftp://123.456.789')
