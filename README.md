## Overview

This Flask application processes images into an image gallery.

## Motivation

TBD

## How to Run

In the top-level directory:

    $ export FLASK_APP=main.py
    $ flask run

Additionally, if you want to run in DEBUG mode which includes auto-loading of source code changes:

    $ export FLASK_ENV=development

To start a Python interpreter with the Flask application automatically loaded:

    $ flask shell

## Key Python Modules Used

- Flask: micro-framework for web application development
- Jinga2 - templating engine
- Flask-SQLAlchemy - ORM (Object Relational Mapper)
- Flask-Migrate - database migration (built on top of alembic)
- Flask-Bcrypt - password hashing
- Flask-Login - support for user management
- Flask-WTF - simplifies forms

This application is written using Python 3.7.

## Testing

TBD