#################
#### imports ####
#################

from flask import render_template, flash, redirect, url_for, request
from flask_login import login_user, current_user, login_required, logout_user, fresh_login_required, confirm_login
from urllib.parse import urlparse

from . import users_blueprint
from .forms import LoginForm, RegistrationForm, ReauthenticateForm, PasswordChangeForm
from project.models import User
from project import db


##########################
#### helper functions ####
##########################

def is_safe_url(target):
    """
    Check if the URL specified is safe to use.

    Within this project, a 'safe' URL is considered to be a relative URL
    with the following properties:
        - Relative URL starting with '/'
        - No scheme (HTTP, HTTPS, FTP, etc.)
        - No net location (ie. www.google.com)

    Examples of safe URLs:
        - /login
        - /user/17

    Examples of unsafe URLs:
        - www.badsite.com
        - https://www.badsite.com
        - ftp://123.456.789
    """
    url = urlparse(target)

    if url.scheme == '' and url.netloc == '' and url.path.startswith('/'):
        return True

    return False


################
#### routes ####
################

@users_blueprint.route('/')
def profile():
    return render_template('users/profile.html')


@users_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    # If the User is already logged in, don't allow them to try to log in again
    if current_user.is_authenticated:
        flash('Already logged in!  Redirecting to your User Profile page...')
        return redirect(url_for('users.profile'))

    form = LoginForm()

    if form.validate_on_submit():
        print(f'Logging in {form.email.data}')
        user = User.query.filter_by(email=form.email.data).first()
        if user and user.check_password(form.password.data):
            user.authenticated = True
            db.session.add(user)
            db.session.commit()
            login_user(user, remember=form.remember_me.data)
            flash('Thanks for logging in, {}!'.format(current_user.email))

            # Check if the page originally attempted to be accessed is provided
            # and use that as the page to redirect to after checking that the
            # page only contains a relative link (ie. within this application)
            next_page = request.args.get('next')
            if not next_page or not is_safe_url(next_page):
                print(f'Invalid next_page: {next_page}')
                next_page = url_for('users.profile')
            else:
                print(f'Valid next_page: {next_page}')
            return redirect(next_page)
        flash('ERROR! Incorrect login credentials.')
    print(f'Form was not validated')
    return render_template('users/login.html', form=form)


@users_blueprint.route('/register', methods=['GET', 'POST'])
def register():
    # If the User is already logged in, don't allow them to try to register
    if current_user.is_authenticated:
        flash('Already registered!  Redirecting to your User Profile page...')
        return redirect(url_for('users.profile'))

    form = RegistrationForm()

    if form.validate_on_submit():
        new_user = User(form.email.data, form.password.data, authenticated=True)
        db.session.add(new_user)
        db.session.commit()
        login_user(new_user)
        flash(f'Thanks for registering, {new_user.email}!')
        return redirect(url_for('users.profile'))
    return render_template('users/registration.html', form=form)


@users_blueprint.route('/logout')
@login_required
def logout():
    user = current_user
    user.authenticated = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    flash('Goodbye!')
    return redirect(url_for('users.profile'))


@users_blueprint.route('/reauthenticate', methods=['GET', 'POST'])
@login_required
def reauthenticate():
    form = ReauthenticateForm()

    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and user.check_password(form.password.data):
            confirm_login()
            flash('Thanks for re-authenticating your account, {}!'.format(current_user.email))
            return redirect(url_for('users.change_password'))
        flash('ERROR! Incorrect login credentials.')
    return render_template('users/reauthenticate.html', form=form)


@users_blueprint.route('/change_password', methods=['GET', 'POST'])
@fresh_login_required
def change_password():
    form = PasswordChangeForm()

    if form.validate_on_submit():
        user = User.query.filter_by(email=current_user.email).first()
        if user:
            user.set_password(form.password.data)
            db.session.add(user)
            db.session.commit()
            flash('Password has been updated, {}!'.format(current_user.email))
            return redirect(url_for('users.profile'))
        flash('ERROR! User could not be found.')
    return render_template('users/password_change.html', form=form)


@users_blueprint.route('/admin')
@login_required
def admin():
    users = User.query.all()
    return render_template('users/admin.html', users=users)
