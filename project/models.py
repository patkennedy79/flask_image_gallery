from flask import current_app, url_for, request
from project import db, bcrypt, images
from PIL import Image as PilImage
import os
import exifread
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer


class ValidationError(ValueError):
    """Class for handling validation errors during
       import of recipe data via API
    """
    pass


class User(db.Model):
    """
    Class that represents a user of the application

    The following attributes of a user are stored in this table:
        - email address
        - hashed password (hashed using Bcrypt)
        - authenticated flag (indicates if a user is logged in or not)

    Attributes to add:
        - date/time created
        - date/time last accessed
    """

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(64), unique=True, index=True)
    hashed_password = db.Column(db.String(128), unique=True, index=True)
    authenticated = db.Column(db.Boolean, index=True)

    def __init__(self, email, plaintext_password, authenticated=False):
        self.email = email
        self.set_password(plaintext_password)
        self.authenticated = authenticated
        print(f'New user created... {self.email}, {self.hashed_password}')

    def __repr__(self):
        return f'<User {self.email}>'

    def set_password(self, plaintext_password):
        self.hashed_password = bcrypt.generate_password_hash(plaintext_password).decode('utf-8')

    def check_password(self, plaintext_password):
        return bcrypt.check_password_hash(self.hashed_password, plaintext_password)

    @property
    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.authenticated

    @property
    def is_active(self):
        """Always True, as all users are active."""
        return True

    @property
    def is_anonymous(self):
        """Always False, as anonymous users aren't supported."""
        return False

    def get_id(self):
        """Return the id of a user to satisfy Flask-Login's requirements."""
        return str(self.id)

    def generate_auth_token(self, expires_in=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expires_in=expires_in)
        return s.dumps({'id': self.id}).decode('utf-8')

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return None
        return User.query.get(data['id'])


class Image(db.Model):
    """
    Class that represents an image

    The following attributes of an image are stored in this table:
        - filename (without the path)
        - filename (with the full path)
        - title

    Attributes to add:
        - date/time created
        - date/time last modified
        - description
    """

    __tablename__ = 'images'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    filename = db.Column(db.String, unique=True, index=True)
    filename_with_path = db.Column(db.String, unique=True, index=True)
    thumbnail_with_path = db.Column(db.String, unique=True, index=True)
    title = db.Column(db.String, index=True)
    description = db.Column(db.String, index=True)
    date_created = db.Column(db.String, index=True)

    def __init__(self, filename=None, filename_with_path=None, title=None, description=None):
        self.filename = filename
        self.filename_with_path = filename_with_path
        if self.filename is not None:
            self.create_thumbnail()
            self.title = title
            self.description = description
            self.extract_creation_date()
            print(f'New image created... {self.filename}, {self.title}')

    def __repr__(self):
        return f'<Image {self.filename_with_path}>'

    def create_thumbnail(self):
        path, file = os.path.split(self.filename_with_path)
        filename, ext = os.path.splitext(self.filename)
        im = PilImage.open(os.path.join(current_app.config['UPLOADS_DEFAULT_DEST'], self.filename))
        im.thumbnail((320, 240))
        im.save(os.path.join(current_app.config['UPLOADS_DEFAULT_DEST'], file + '_thumbnail' + ext))
        self.thumbnail_with_path = os.path.join(path, file + '_thumbnail' + ext)

    def extract_creation_date(self):
        # Open the image file for reading the meta data (binary mode)
        f = open(os.path.join(current_app.config['UPLOADS_DEFAULT_DEST'], self.filename), 'rb')
        print(f'Opening {os.path.join(current_app.config["UPLOADS_DEFAULT_DEST"], self.filename)}')

        # Get the Exif tags of the image
        exif_tags = exifread.process_file(f, details=False)

        for tag in exif_tags.keys():
            print(f'\tTag: {tag}: {exif_tags[tag]}')
            if tag == 'EXIF DateTimeOriginal':
                date_file = str(exif_tags[tag])
                photo_date = date_file[0:10]
                self.date_created = str.replace(photo_date, ":", "-")

    def get_url(self):
        return url_for('gallery_api.api1_0_get_image', image_id=self.id, _external=True)

    def export_data(self):
        return {
            'self_url': self.get_url(),
            'filename': self.filename,
            'title': self.title,
            'description': self.description,
            'date_created': self.date_created,
        }

    def import_data(self, request):
        """Import the data for this image by either saving the image or saving
        the metadata associated with the image. If the metadata is being processed,
        the title of the image must always be specified."""
        try:
            if 'image' in request.files:
                print(f'Found image in request.files!')
                filename = images.save(request.files['image'])
                self.filename = filename
                self.filename_with_path = images.url(filename)
                self.create_thumbnail()
                self.extract_creation_date()
                print(f'Imported data... {self.filename}, {self.filename_with_path}')
            else:
                print(f'Processing metadata!')
                json_data = request.get_json()
                self.title = json_data['title']
                if 'description' in json_data:
                    self.description = json_data['description']
                print(f'Imported data... {self.title}, {self.description}')
        except KeyError as e:
            raise ValidationError('Invalid image: missing ' + e.args[0])
        return self
