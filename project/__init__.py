from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_uploads import UploadSet, IMAGES, configure_uploads
from flask_httpauth import HTTPBasicAuth


#######################
#### Configuration ####
#######################

# Create the instances of the Flask extensions (flask-sqlalchemy, flask-login, etc.) in
# the global scope, but without any arguments passed in.  These instances are not attached
# to the application at this point.
db = SQLAlchemy()
migrate = Migrate()
bcrypt = Bcrypt()
login_manager = LoginManager()
images = UploadSet('images', IMAGES)
auth = HTTPBasicAuth()
auth_token = HTTPBasicAuth()


######################################
#### Application Factory Function ####
######################################

def create_app(config_filename=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_pyfile(config_filename)
    initialize_extensions(app)
    register_blueprints(app)
    return app


##########################
#### Helper Functions ####
##########################

def initialize_extensions(app):
    db.init_app(app)
    migrate.init_app(app, db)
    bcrypt.init_app(app)
    login_manager.init_app(app)
    login_manager.login_view = "users.login"
    login_manager.refresh_view = "users.reauthenticate"
    login_manager.needs_refresh_message = (
        u"To protect your account, please re-authenticate to access this page."
    )

    # Flask-Login configuration
    from project.models import User

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.filter(User.id == int(user_id)).first()

    # Configure the image uploading via Flask-Uploads
    configure_uploads(app, images)


def register_blueprints(app):
    # Since the application instance is now created, register each Blueprint
    # with the Flask application instance (app)
    from project.users import users_blueprint
    from project.gallery import gallery_blueprint
    from project.gallery_api import gallery_api_blueprint

    app.register_blueprint(users_blueprint)
    app.register_blueprint(gallery_blueprint)
    app.register_blueprint(gallery_api_blueprint)


#######################
#### Models Import ####
#######################

# Importing the models here in project/__init__.py is necessary to
# allow Flask-Migrate/Alembic to find the database models
from project import models
