#################
#### imports ####
#################

from flask import render_template, Blueprint, request, redirect, url_for, abort, jsonify, g, current_app
from project import db, auth, auth_token
from project.models import Image, User
from .decorators import no_cache, etag
from . import gallery_api_blueprint


##########################
#### helper functions ####
##########################

@auth.verify_password
def verify_password(email, password):
    g.user = User.query.filter_by(email=email).first()
    # if g.user.role != 'admin':
    #     return False
    # if g.user is None:
    #     return False
    return g.user.check_password(password)


@auth_token.verify_password
def verify_authentication_token(token, unused):
    g.user = User.verify_auth_token(token)
    return g.user is not None


########################
#### error handlers ####
########################

@gallery_api_blueprint.errorhandler(404)
def api_error(e):
    response = jsonify({'status': 404, 'error': 'not found (API!)', 'message': 'invalid resource URI'})
    response.status_code = 404
    return response


@gallery_api_blueprint.errorhandler(405)
def api_error(e):
    response = jsonify({'status': 405, 'error': 'method not supported (API!)', 'message': 'method is not supported'})
    response.status_code = 405
    return response


@gallery_api_blueprint.errorhandler(500)
def api_error(e):
    response = jsonify({'status': 500, 'error': 'internal server error (API!)', 'message': 'internal server error occurred'})
    response.status_code = 500
    return response


@auth.error_handler
def unauthorized():
    response = jsonify({'status': 401, 'error': 'unauthorized',
                        'message': 'please authenticate'})
    response.status_code = 401
    return response


@auth_token.error_handler
def unauthorized_token():
    response = jsonify({'status': 401, 'error': 'unauthorized',
                        'message': 'please send your authentication token'})
    response.status_code = 401
    return response


################
#### routes ####
################

@gallery_api_blueprint.before_request
# @auth_token.login_required
def before_request():
    """All routes in this blueprint require authentication."""
    pass


@gallery_api_blueprint.after_request
@etag
def after_request(rv):
    """Generate an ETag header for all routes in this blueprint."""
    return rv


@gallery_api_blueprint.route('/get-auth-token')
@auth.login_required
@no_cache
def get_auth_token():
    return jsonify({'token': g.user.generate_auth_token()})


@gallery_api_blueprint.route('/api/v1_0/images', methods=['GET'])
@auth_token.login_required
def api1_0_get_all_images():
    return jsonify({'images': [image.get_url() for image in Image.query.all()]})


@gallery_api_blueprint.route('/api/v1_0/images/<int:image_id>', methods=['GET'])
@auth_token.login_required
def api1_0_get_image(image_id):
    return jsonify(Image.query.get_or_404(image_id).export_data())


@gallery_api_blueprint.route('/api/v1_0/images', methods=['POST'])
def api1_0_create_image():
    new_image = Image()
    new_image.import_data(request)
    db.session.add(new_image)
    db.session.commit()
    return jsonify({}), 201, {'Location': new_image.get_url()}


@gallery_api_blueprint.route('/api/v1_0/images/<int:image_id>', methods=['PUT'])
def api1_0_update_image(image_id):
    current_image = Image.query.get_or_404(image_id)
    current_image.import_data(request)
    db.session.add(current_image)
    db.session.commit()
    return jsonify({'result': 'True'})


@gallery_api_blueprint.route('/api/v1_0/images/<int:image_id>', methods=['DELETE'])
def api1_0_delete_image(image_id):
    current_image = Image.query.get_or_404(image_id)
    db.session.delete(current_image)
    db.session.commit()
    return jsonify({'result': True})
