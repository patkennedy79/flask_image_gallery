"""
The gallery_api Blueprint handles processing images through a REST API.
"""
from flask import Blueprint
gallery_api_blueprint = Blueprint('gallery_api', __name__, template_folder='templates')

from . import routes
