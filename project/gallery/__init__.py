"""
The gallery Blueprint handles displaying images.
"""
from flask import Blueprint
gallery_blueprint = Blueprint('gallery', __name__, template_folder='templates')

from . import routes
