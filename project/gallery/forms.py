from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Optional
from flask_wtf.file import FileField, FileAllowed, FileRequired
from project import images


class AddImageForm(FlaskForm):
    image = FileField('Recipe Image', validators=[FileRequired(), FileAllowed(images, 'Images only!')])
    image_title = StringField('Title', validators=[DataRequired()])
    image_description = StringField('Description', validators=[Optional()])
    submit = SubmitField('Add')


class EditImageForm(FlaskForm):
    image_title = StringField('Title', validators=[DataRequired()])
    image_description = StringField('Description', validators=[Optional()])
    submit = SubmitField('Update')
