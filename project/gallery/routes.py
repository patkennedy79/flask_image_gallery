#################
#### imports ####
#################

from flask import render_template, url_for, request, flash, redirect
from flask_login import login_required

from . import gallery_blueprint
from .forms import AddImageForm, EditImageForm
from project.models import Image
from project import images, db


################
#### routes ####
################

@gallery_blueprint.route('/images')
@login_required
def gallery():
    image_gallery = Image.query.all()
    return render_template('gallery/images.html', images=image_gallery)


@gallery_blueprint.route('/image/<image_id>')
def image_view(image_id):
    image = Image.query.filter_by(id=image_id).first_or_404()
    return render_template('gallery/image_view.html', image=image)


@gallery_blueprint.route('/add_image', methods=['GET', 'POST'])
@login_required
def add_image():
    # Cannot pass in 'request.form' to AddImageForm constructor, as this will cause 'request.files' to not be
    # sent to the form.  This will cause AddImageForm to not see the file data.
    # Flask-WTF handles passing form data to the form, so not parameters need to be included.
    form = AddImageForm()

    if request.method == 'POST' and form.validate_on_submit():
        filename = images.save(request.files['image'])
        url = images.url(filename)
        new_image = Image(filename, url, form.image_title.data, form.image_description.data)
        db.session.add(new_image)
        db.session.commit()
        flash(f'New image, {form.image_title.data}, added!')
        return redirect(url_for('gallery.gallery'))

    return render_template('gallery/add_image.html', form=form)


@gallery_blueprint.route('/edit_image/<image_id>', methods=['GET', 'POST'])
@login_required
def edit_image(image_id):
    form = EditImageForm()
    current_image = Image.query.filter_by(id=image_id).first_or_404()

    if form.validate_on_submit():
        current_image.title = form.image_title.data
        current_image.description = form.image_description.data
        db.session.add(current_image)
        db.session.commit()
        flash(f'Image, {form.image_title.data}, has been updated!')
        return redirect(url_for('gallery.gallery'))

    return render_template('gallery/edit_image.html', form=form, image=current_image)


@gallery_blueprint.route('/delete_image/<image_id>', methods=['GET', 'POST'])
@login_required
def delete_image(image_id):
    current_image = Image.query.filter_by(id=image_id).first_or_404()
    image_title = current_image.title
    db.session.delete(current_image)
    db.session.commit()
    flash(f'Image ({image_title}) was deleted.')
    return redirect(url_for('gallery.gallery'))
