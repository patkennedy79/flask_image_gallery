##########################################################
#
# configuration_test.py is intended to be used for testing a Flask application
#
##########################################################
import os


# Get the folder of the top-level directory of this project
BASEDIR = os.path.abspath(os.path.dirname(__file__))

# Secret Key
SECRET_KEY = 'TEST_SECRET_KEY'

# SQLAlchemy
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASEDIR, 'test.db')
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Bcrypt algorithm hashing rounds (reduced for testing purposes only!)
BCRYPT_LOG_ROUNDS = 4

# Enable the TESTING flag to disable the error catching during request handling
# so that you get better error reports when performing test requests against the application.
TESTING = True

# Disable CSRF tokens in the Forms (only valid for testing purposes!)
WTF_CSRF_ENABLED = False
