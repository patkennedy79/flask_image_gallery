from project import create_app

# Call the Application Factory function to construct a Flask application instance
# using the standard configuration defined in /instance/configuration_full.py
app = create_app('configuration_full.py')
